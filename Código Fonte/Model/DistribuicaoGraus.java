package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Método que calcula a distribuição de graus de uma rede livre de escalas. Utilizando a seguinte lei de potencia
 * pk' ~~ c*k^-alfa, onde alfa é um valor definido, k o grau do vértice utilizado.
 * @author Lucas Soares
 */
public class DistribuicaoGraus implements Serializable {
    Grafo g;
    
    
    /**
     * Método construtor que recebe o grafo da aplicação.
     * @param g  - Grafo passado como parametro.
     */
    public DistribuicaoGraus(Grafo g) {
        this.g = g;
    }
    
    /**
     * Método que cálcula a lei de potência para a distribuição real dos dados. Passa-se o 
     * valor do parâmetro k(variável aleatória).
     * Fórmula utilizada: <b>f(x) = c*(x^-alfa)</b>.
     * @param k - Variável aleatória.
     * @return Double - Retorna o resultado da operação da lei de potência.
     */
    public double leidePotencia(int k) {
        double alfa = 0.5;
        double c = 1.0;
        
        if(k == 0)
           return 0;
        

        return c*(Math.pow(k, -alfa));
    }
    
    /**
     * Método que cálcula a lei de potência para o gráfico log-log.
     * @param k - Variável aleatória.
     * @return Double - Retorna o resultado da operação da lei de potência.
     */
    public double leidePotenciaPowerLaw(int k) {
       double alfa = 1.0;
       double c = 1.0;
       return c - alfa*(Math.log(k));   
    }
    
    /**
     * Método que calcula a distribuição de graus e retorna o valor da distribuição de graus.
     * @return Double - Somatorio da distribuição de probabilidade, aplicando a powerLaw a
     * todos os graus do grafo.
     */
    public double calcularDistribuicao() {
        double pk = 0;
        
        
        for(int i = 0; i <= g.grauMax(); i++) {
            pk = pk + this.leidePotencia(g.quantGrauK(i));            
        }
        return pk/g.quantVertices();    
    }
    
    /**
     * Metodo que calcula a distribuição de graus para cada grau do grafo
     * e retorna a lista de todos os valores, para plotagem do gráfico.
     * @return distri - Lista com todas as distribuições de graus.
     */
    public ArrayList<Double> calcularDistibuicao() {
        ArrayList<Double> distri = new ArrayList<>();
        for(int i = 0; i <= g.grauMax(); i++) {
            distri.add(leidePotencia(g.quantGrauK(i)));
        }        
        return distri;
    }
    
    /**
     * Metodo que calcula e retorna o histograma da relacao dos graus da rede com a quantidade de vertices. 
     * @return HashMap - Histograma com o grau e com sua respectiva quantidade de vertices.
     */
    public HashMap retornarHistograma() {
        HashMap<Integer, Integer> histograma = new HashMap<>();
        for(int i = 0;i <= g.grauMax(); i++)
            if(g.quantGrauK(i) != 0) 
                histograma.put(i, g.quantGrauK(i));
           
       return histograma;         
    }
}
