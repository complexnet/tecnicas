/**
 * Classe responsável por calcular o coeficiente de agregação de um grafo.
 * 
 *<p>O coeficiente de agregação é o rácio entre as ligações existentes conectando os vizinhos do nó uns aos outros.<br />
 * O coeficiente de agregação para toda rede é a média dos coeficientes de todos os nós de agregação.<br /> 
 * Um alto coeficiente de agregação significa que a rede está muito conectada.<br /></p>
 * 
 * <b>Fórmula</b><br /> 
 * <b>(2*Ei)/(Gi(Gi-1))</b>
 * 
 * @author Felipe Sobreira
 */

package Model;

import java.io.Serializable;

public class CoefAgregacao implements Serializable {
       
    double coefVertice = 0.0;
    double coefRede = 0.0;
    
    public CoefAgregacao() {
        
    }
    
    /**
     * Método que cálcula o coeficiente de agregação de um único vértice passado. Passando um único vértice pro método<br />
     * Utiliza os métodos {@link #linkEntreVizinhos}, {@link Vertice.#quantVizinhos}. 
     * @param no - Vértice do grafo
     * @return Double - Coeficiente de agregação de um único vértice.
     */
    double coefVertice(Vertice no){
        if(no.calcularGrau() > 1)
            coefVertice = (2*linkEntreVizinhos(no))/( no.calcularGrau()*( no.calcularGrau() - 1 ));
        else{
            coefVertice = 0.0;
        }
        return coefVertice;
    }
    
    /** 
     * Método que calcula o coeficiente de agregação de toda rede. Fazendo um somátorio da fórmula definida como <b>(2*Ei)/(Gi(Gi-1))</b>.<br />
     * Utiliza os seguintes métodos {@link Grafo.#retornaLista} e {@link Grafo.#quantVertices}.
     * @param g - Grafo da rede, passado por parametro.
     * @return Double - Coeficiente de agregação de toda rede.
     */
    double coefRede(Grafo g){
        for(Vertice v: g.getListaVertices()){
            coefRede = coefRede + coefVertice(v);
        }
        return coefRede/g.quantVertices();
    }
    
    /**
     * Método que cálcula a quantidade de arestas entre os vizinhos de um vértice. Utilizando de dois laços
     * verificando se dois vértices estão na vizinhança do outro.<br />
     * Utiliza os seguintes métodos {@link Vertice.#retornaLista} e {@link  Vertice.#verificarVizinho}. 
     * @param no - Vértice do grafo.
     * @return Double - Retorna a quantidade de arestas entre os seus vizinhos.
     */
    double linkEntreVizinhos(Vertice no){
        
        double link = 0.0;
        
        for(Vertice v: no.getVizinhanca()){
            for(Vertice k: no.getVizinhanca()){
                if(v.verificarVizinho(k)){
                    link++;
                }
            }
        } 
        //O RETORNO É FEITO ASSIM POR CAUSA DA INEFICIÊNCIA DO PERCURSO FEITO NO FOR ANTERIOR
        
        return (link-(link/2));
        
    }
    
    
    
}
