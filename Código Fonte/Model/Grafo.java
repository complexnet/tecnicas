package Model;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * Classe que contém o modelo do grafo, onde vai conter todos os vértices pertencentes ao grafo.
 * @author Bruno Menezes, Lucas Soares, Rennan Rabelo e Felipe Sobreira
 */
public class Grafo implements Serializable
{  
    protected ArrayList<Vertice> listaVertices = new ArrayList<Vertice>();
    
    /**
     * Método responsável por adicionar um vértice a rede, ou seja, adicionar um vértice a coleção utlizada.
     * @param v - Vértice que deve ser adicionado.
     * @return Boolean - Resultado para saber se o vértice foi adicionado com sucesso, ou  ocorreu algum problema.
     */
    public boolean adicionarVertice(Vertice v) 
    {
        return listaVertices.add(v);    
    }
    
   /**
    * Método responsável por remover um vértice da rede, ou seja, remover um vértice da coleção utilizada.
    * O vértice também é removido da vizinhança de seus vizinhos.
    * 
    * @param v - Vértice que deve ser removido.
    * @return Boolean - Resultado para saber se o vértice foi removido com sucesso, ou ocorreu algum problema.
    */    
    public boolean removerVertice(Vertice v) 
    {
        boolean verificar = listaVertices.remove(v);
        if(verificar)
        {
            ArrayList<Vertice> vizinhosV = v.getVizinhanca();
            for(Vertice u: vizinhosV)
            {
                u.removerVizinho(v);
            }
        }
        return verificar;
    }
    
     /**
     * Método para remover um vértice da rede, ou seja, da coleção ArrayList que armazena os vértices, segundo o seu
     * índice nesta coleção.
     * 
     * @param index - Índice do vértice que se deseja remover.
     * @return a - Variável que indica se a remoção ocorreu ou não.
     */
    public boolean removerVertice(int index) 
    {
        boolean a = true;
        if(listaVertices.contains(listaVertices.get(index)))
        {
            listaVertices.remove(index);
        }
        else
        {
            a = false;
        }
        return a;
    }
    
    /**
     * Método para adicionar uma aresta.
     * 
     * @param u - Primeira extremidade da aresta.
     * @param v - Segunda extremidade da aresta.
     * @return verificar - Variável que indica se a adição da aresta ocorreu ou não.
     * 
     */
    public boolean adicionarAresta(Vertice v, Vertice u)
    {
        return v.adicionarVizinho(u);
    }
    
    /**
     * Método para remover uma aresta.
     * 
     * @param u - Primeira extremidade da aresta.
     * @param v - Segunda extremidade da aresta.
     * @return verificar - Variável que indica se a aresta foi ou não removida.
     * 
     */
    public boolean removerAresta(Vertice v, Vertice u)
    {
        return v.removerVizinho(u);
    }
    
    /**
     * Método que retorna um vértice do grafo, ou seja, retorna um vértice da coleção utilziada, 
     * passando o respectivo índice.
     * @param index - Índice do vértice que se quer retornar.
     * @return Vertice - Retorna o vértice desejado.
     */
    public Vertice getVertice(int index)
    {
        return listaVertices.get(index);
    }
    
    /**
     * Método que retorna um índice de determinado vértice, em uma coleção. 
     * @param v - Vértice que se deseja recuperar seu índice.
     * @return Int - Retorna o índice do vértice passado, na coleção.
     */
    public int getVerticeId(Vertice v)
    {
        return listaVertices.indexOf(v);
    }
    
    /**
     * Método que retorna a quant de vértices que existem no grafo.
     * @return Int - Retorna a quantidade de vértices existentes na coleção utilizada. 
     */
    public int quantVertices()
    {
        return listaVertices.size();
    }
    
    /**
     * Método usado para retornar o hashcode de cada objeto.
     * No projeto, método usado para debugar o código.
     */
    public void percorrerVertices()
    {
        for(Vertice v: listaVertices)
        {
            System.out.println(v.toString());
        }
    }

    /**
     * Método retorna todos os vértices do grafo.
     * @return listaVertices - Lista de vértices do grafo. 
     * 
     */
    public ArrayList<Vertice> getListaVertices()
    {
        return listaVertices;
    }
    
    /**
     * Método cálcla a quantidade de arestas no grafo, fazendo um somátorio(laço for) das arestas de cada vértice.
     * Utiliza o método calcularGrau da classe Vertice.
     * @return Int - Retorna a quantidade de arestas no grafo.
     */
    public int quantArestas() 
    {
        int somatorioGrau = 0;       
        for(Vertice v: listaVertices)
        {
           somatorioGrau += v.calcularGrau();
        }
        return (int)somatorioGrau/2;
    }
    
    /**
     * Método que cálcula a quantidade máxima de arestas possiveis no grafo. Utilizando a seguinte fórmula 
     * <b> (q*(q - 1))/2</b> . 
     * @return Int - Retornar a quantidade máxima possível de arestas.
     */
    public int quantMaxArestas() 
    {
        int q = this.quantVertices();
        return (q*(q-1))/2;
    }
    
    /**
     * Método que retorna o grau do vértie com maior grau no grafo.
     * @return Int - Retorna o grau maximo dos vértices do grafo.
     */
    public int grauMax() 
    {
        int max = 0;
        for(Vertice v : listaVertices)
        {
            if(v.calcularGrau() > max)
            {
                max = v.calcularGrau();
            }
        }
        return max;
    }
    
    /**
     * Método que retorna q auntidade de vértices que possuem o grau que se deseja.
     * @param k - Valor do grau que se quer comparar.
     * @return Int - Retorna a quantidade de vértices com o grau k.
     */
    public int quantGrauK(int k) 
    {
        int quant = 0;
        for(Vertice v: listaVertices) 
        {
            if(v.calcularGrau() == k)
            {
                quant++;
            }
        }
        return quant;
    }
    
    /**
     * Método que retorna o somatorio de todos os graus
     * @return Int - Retorna a soma dos graus.
     */
    public int grauTotal() 
    {
        int quant = 0;
        for(Vertice v: listaVertices) 
        {
           quant += v.calcularGrau();
        }
        return quant;
    }    
}