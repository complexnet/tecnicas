package Model;

import java.io.Serializable;

/**
 * <p>Calculamos o diâmetro do grafo utilizando o algoritmo de programação dinâmica de Floyd-Warshall.
 * Depois de aplicar o algoritmo, encontramos a maior distância calculada para um par de vértices.
 * Diâmetro: maior distância de um grafo.
 * Distância: tamanho do menor caminho entre dois vértices de um grafo.</p>
 * 
 * @author Bruno Menezes
 *
 */

public class Diametro implements Serializable
{
    private Grafo g;
    private double pesosW[][];
    private final static double INFINITO = 1000000;
    
    /**
     * Construtor.
     * 
     * Configura o grafo do objeto como sendo novoGrafo e instancia a matriz de pesos das arestas.
     * 
     * @param novoGrafo - Nova instância de Grafo cujo o diâmetro deve ser calculado.
     * 
     */
    public Diametro(Grafo novoGrafo)
    {
        setGrafo(novoGrafo);
    }
    
    /** 
     * Método que adiciona o grafo que vai se utilizar na classe.
     *
     * @param novoGrafo - Grafo utilizado na aplicação. 
     */
    public void setGrafo(Grafo novoGrafo)
    {
        g = novoGrafo;
        int n = g.quantVertices();
        double aux[][] = new double[n][n];
        pesosW = aux;
    }
    
    /**
    * Método que retona o menor entre dois valores double.
    * 
    * @param x - Primeiro double a ser comparado.
    * @param y - Segundo double a ser comparado.
    * @return - O mínimo entre x e y.
    *
    */
    public double minimo(double x, double y)
    {
        if(x < y)
        {
            return x;
        }
        
        return y;
    }
    
    /**
    * Método que define os pesos das arestas do grafo. 
    * Nesta aplicação, todas as arestas possuem peso 1.
    *
    */
    private void definirPesos()
    {
        int i, j, n = g.quantVertices();
        Vertice aux;
        
        for(i = 0;i < n;i++)
        {
            aux = g.getVertice(i);
            for(j = 0;j < n;j++)
            {
                if(i == j)
                {
                    pesosW[i][j] = 0;
                }
                if(i != j)
                {
                    if(aux.verificarVizinho(g.getVertice(j)))
                    {
                        pesosW[i][j] = 1;
                    }
                    else
                    {
                        pesosW[i][j] = INFINITO;
                    }
                }
            }
        }
    }
    
    /**
    * Método que calcula os caminhos mínimos (distâncias) entre todos os pares de vértices usando o algoritmo de Floyd-Warshall.
    * @return D - Matriz com as distâncias de um vértice i para um vértice j.
    *
    */
    public double[][] FloydWarshall()
    {
        this.definirPesos();
        int i, j, k, n = g.quantVertices();
        double D[][] = pesosW;
        
        for(k = 0;k < n;k++)
        {
            for(i = 0;i < n;i++)
            {
                for(j = 0;j < n;j++)
                {
                    D[i][j] = minimo(D[i][j], D[i][k] + D[k][j]);
                }
            }
        }
        
        return D;
    }
    
    /**
    * Método que calcula o diâmetro em si.
    * Aplica o algoritmo de FloydWarshall e, em seguida, busca a maior distância retornada pelo algoritmo.
    * @return max - maior distância entre dois vértices no grafo, ou seja, o diâmetro.
    *
    */
    public double calcularDiametro()
    {
        int i, j, n = g.quantVertices();
        double D[][] = this.FloydWarshall();
        double max = 0;
        
        for(i = 0;i < n;i++)
        {
            for(j = 0;j < n;j++)
            {
                if(D[i][j] != INFINITO)
                {
                    if(D[i][j] > max)
                    {
                        max = D[i][j];
                    }
                }
            }
        }
        
        return max;
    }
    
    /**
    * Método que exibe no console os pesos das arestas entre todos os pares de vértices do grafo.
    *
    */
    public void percorrerPesos()
    {
        int i, j, n = g.quantVertices();
        
        for(i = 0;i < n;i++)
        {
            for(j = 0;j < n;j++)
            {
                System.out.println("("+i+","+j+")"+pesosW[i][j]);
            }
        }
    } 
}
