package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import Utility.Mensagens;

/**
 * <p>Calcula o caminho mínimo de um vértice fonte aos outros utilizando o algoritmo de busca em largura.
 * Além das distâncias, também calcula os predecessores, sendo possível assim construir os caminhos mínimos
 * de um vértice fonte aos demais.</p>
 * 
 *<p> O algoritmo de busca em largura (BFS) aqui implementado baseia-se no pseudo-código do algoritmo do livro
 * Introduction to Algorithms, de Thomas H. Cormen, Charles E. Leiserson, Ronald L. Rivest, and Clifford Stein.
 * Note que não precisamos usar a cor preto, mas optamos por mantê-la. Talvez alguma aplicação futura, e.g, 
 * imprimir os passos da busca para o usuário, necessite usar a cor preta.</p>
 * 
 * @author Bruno Menezes
 */

public class CaminhoMinimo implements Serializable
{
    private Grafo g;
    private Vertice fonte;
    private static final int BRANCO = 0;
    private static final int CINZA = 1;
    private static final int PRETO = 2;
    private static final int INFINITO = 99999999;
    private ArrayList<Integer> listaCores;
    private Hashtable<Integer, Integer> listaDistancia;
    private ArrayList<Vertice> listaPredecessores;
    
    /**
     * Construtor que inicializa todos os atributos(grafo, listaCores, listaDistancia, listaPredecessores) para poder calcular o caminho minimo.
     * @param graph - Grafo da rede.
     */
    public CaminhoMinimo(Grafo graph)
    {
        g = graph;
        fonte = null;
        listaCores = new ArrayList<Integer>();
        listaDistancia = new Hashtable<Integer, Integer>();
        listaPredecessores = new ArrayList<Vertice>();
    }
    
    /**
     * Construtor que inicializa todos os atributos(grafo, fonte, listaCores, listaDistancia, listaPredecessores) para o cálculo do caminho minimo.
     * @param graph - Grafo da rede.
     * @param source  - Vertice da fonte.
     */
    public CaminhoMinimo(Grafo graph, Vertice source)
    {
        g = graph;
        fonte = source;
        listaCores = new ArrayList<Integer>();
        listaDistancia = new Hashtable<Integer, Integer>();
        listaPredecessores = new ArrayList<Vertice>();
    }
    
    /**
     * Método que adiciona o vertice fonte.
     * @param novaFonte  - Vertice fonte.
     */
    public void setFonte(Vertice novaFonte)
    {
        if(g == null)
        {
            Mensagens.mensagemAlertaConsole("Não existe um grafo instanciado."); 
            return;
        }
        
        fonte = novaFonte;
        listaCores.clear();
        listaDistancia.clear();
        listaPredecessores.clear();
    }
    
    /**
     * Método que adiciona um grafo ao objeto.
     * @param graph - Grafo da rede. 
     */
    public void setGrafo(Grafo graph)
    {
        g = graph;
    }
    
    /**
     * Método que instancia todos os atributos dos vertices, adicionando suas respectivas cores e predecessores
     */
    private void instanciarAtributos()
    {
        for(int i = 0; i < g.quantVertices(); i++)
        {
            if(g.getVertice(i) != fonte)
            {
                listaCores.add(i, BRANCO);
                listaDistancia.put(i, INFINITO);
            }
            else
            {
                listaCores.add(i, CINZA);
                listaDistancia.put(i, 0);
            }
            listaPredecessores.add(i, null);
        }
    }
    
    /**
     * Método que implementa uma busca em largura no grafo, a partir do vertice fonte. Lança uma exceção caso
     * vertice fonte não esteja definido.
     * @throws Exception 
     */
    public void buscaEmLargura() throws Exception
    {
        if(fonte == null)
        {
            throw new Exception("Vértice fonte não definido.");
        }
        
        int indexVizinho, indexU;
        Vertice u;
        ArrayList<Vertice> vizinhanca;
        ArrayList<Vertice> fila = new ArrayList<Vertice>();
        this.instanciarAtributos();
        
        fila.add(fonte);

        while(!fila.isEmpty())
        {
            u = fila.remove(0);
            indexU = g.getVerticeId(u);
            vizinhanca = u.getVizinhanca();
            
            for(Vertice v: vizinhanca)
            {
                indexVizinho = g.getVerticeId(v);
                if(listaCores.get(indexVizinho) == BRANCO)
                {
                    listaCores.set(indexVizinho, CINZA);
                    listaDistancia.put(indexVizinho, listaDistancia.get(indexU) + 1);
                    listaPredecessores.set(indexVizinho, u);
                    fila.add(v);
                }
            }
         
            listaCores.set(indexU, PRETO);
        }
    }
    
    /**
     * Método que retorna a lista de distancias, do vertice fonte a os demais vertices.
     * @return HashTable - lista de distancia.
     */
    public Hashtable<Integer, Integer> distanciaMinima()
    {   
        return listaDistancia;
    }
    
    /**
     * Método que retorna a lista de predecessores do caminho minimo.
     * @return ArrayList - lista de predecessores.
     */
    public ArrayList<Vertice> predecessoresCaminhoMinimo()
    {
        return listaPredecessores;
    }
    
    /**
     * Método que imprime o caminho minimo de um determinado vertice passado.
     * @param v - Vertice utilizado.
     */
    public void imprimirCaminho(Vertice v)
    {
        if(v.equals(fonte))
        {
            System.out.println("Vértice: "+v.verticeID());
        }
        else
        {
            if(listaPredecessores.get(g.getVerticeId(v)) == null)
            {
                   Mensagens.mensagemAlertaGUI("Não existe caminho de "+fonte.verticeID()+" para "+v.verticeID()+".");
            }
            else
            {
                imprimirCaminho(listaPredecessores.get(g.getVerticeId(v)));
                System.out.println("Vértice :"+v.verticeID());
            }
        }
    }
}