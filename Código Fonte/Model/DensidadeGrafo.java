package Model;

import java.io.Serializable;

/**
 * <p>Classe responsável por calcular a densidade de um grafo.</p>
 * <p>A densidade D de uma rede é definida como uma relação entre o número de arestas E para o número de arestas possíveis,
 *  dada pelo coeficiente binomial (N 2), sendo a fórmula <b>D = 2*E/(N*(N-1))</b>.</p>
 * <p>Temos a utilização de dois métodos da classe grafo, para poder calcular a densidade. Sendo elas quantArestas
 * e quantMaxArestas. </p>
 * 
 * @author Rennan Costa
 */

public class DensidadeGrafo implements Serializable {
    private Grafo g;
    /**
     * Método que recebe o objeto grafo.
     * @param novoGrafo - Recebe o grafo utilizado na Rede.
     */  
    public void setGrafo(Grafo novoGrafo)
    {
        g = novoGrafo;
    }
    
    /**
     * Método que cálcula a densidade do grafo, utlizando métodos da classe Grafo. Sendo elas {@link Grafo#quantArestas} e {@link Grafo#quantMaxArestas}
     * @return Float - Valor da densidade do grafo. 
     */
    public float calcularDensidade(){
        return (float) g.quantArestas()/g.quantMaxArestas();  // Sem o cast para float, a divisão dará um número inteiro.
    }
}
