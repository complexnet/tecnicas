package Model;

import java.io.Serializable;
import java.util.ArrayList;
import Utility.Mensagens;

/**
 * <p>Classe para objetos do tipo Vértice, contem os seus métodos e atributos. A classe foi implementado
 * utilizando a coleção ArrayList, para melhor recuperação dos dados dos vértices.</p>
 * <p>Sempre quando fizer referencia ao objeto que chama o método, será usado a nomencleatura de vértice u, 
 * e o vértice vizinho chamado de v.</p>
 * 
 * @author Bruno Menezes, Lucas Soares, Rennan Costa
 */


public class Vertice implements Serializable{	

    protected ArrayList<Vertice> listaVizinhos = new ArrayList<Vertice>();
    
    /**
     * Método que adiciona um vértice vizinho a outro. Como se estivesse adicionando uma aresta
     * entre os dois vértices.
     * @param vizinho - Vértice vizinho que vai ser adicionado a lista.
     * @return Boolean -  Valor true ou false, que verifica se foi adicionado com sucesso.
     */
    public boolean adicionarVizinho(Vertice vizinho)
    {
        boolean verificar = false;
        
        if(!listaVizinhos.contains(vizinho))
        {
            verificar = this.listaVizinhos.add(vizinho);
        }
        if(!vizinho.verificarVizinho(this))
        {
            verificar = vizinho.adicionarVizinho(this);
        }
        
        return verificar;
    }
    
    /**
     * Método que remove um vértice vizinho a outro. Como se estivesse removendo uma aresta
     * entre dois vértices.
     * @param vizinho - Vértice vizinho que vai ser removido da lista.
     * @return Boolean - Valor true ou false, que verifica se foi removido com sucesso.
     */
    public boolean removerVizinho(Vertice vizinho)
    {
        boolean verificar = false;
               
        if(!listaVizinhos.contains(vizinho)) {
            Mensagens.mensagemAlertaGUI("Vértice passado não existe"); 
        } else {
         verificar = this.listaVizinhos.remove(vizinho);
         verificar = vizinho.removerVizinho(this);
        }
        return verificar;
    }
    
    /**
     * Método que verifica se existe o vértice especificado na lista de Vizinhos.
     * @param vizinho - Vértice utilizado na verificação.
     * @return Boolean - Valor true ou false, para saber se o vertice está contido na lista de Vizinhos ou não.
     */
    public boolean verificarVizinho(Vertice vizinho)
    {
        return listaVizinhos.contains(vizinho);
    }
    
    /**
     * Método que retorna o nome do indice do objeto.
     * @return String - Nome do indice objeto
     */
    public String verticeID()
    {
        return this.toString();
    }
    
    /**
     * Método que percorre todos os vizinhos do vértice. Ou seja, suas lista de vizinhos. E printa o nome
     * dos objetos na tela.
     */
    public void percorrerVizinhos()
    {
        for(Vertice v: listaVizinhos)
        {
            System.out.println(v.toString());
        }
    }
    
    /**
     * Método que retorna um vizinho do vértice u, passando o índice do vértice v.
     * @param index - Índice do vértice que se quer retornar.
     * @return Vertice - Retorna o vértice desejado.
     */
    public Vertice Vizinhos(int index) {
        
        return listaVizinhos.get(index);
        
    }
    
    /**
     * Método que retorna todos os vizinhos de um determinado vértice. Ou seja, 
     * retorna a lista de vizinhos do vértice.
     * 
     * @return listaVizinhos - Retorna todos os vizinhos de u.
     */    
    public ArrayList<Vertice> getVizinhanca()
    {
        return listaVizinhos;
    }
    
    /**
     * Método que retorna o tamanho da lista de Vizinhos de um vértice. Também pode ser interpretado como
     * a quantidade de arestas de um vértice.
     * @return Int - Retorna o valor do grau de um vértice. 
     */
    public int calcularGrau()
    {
        return listaVizinhos.size();
    }
    
}
