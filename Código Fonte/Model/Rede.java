package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;
import java.util.HashMap;
import Observer.ObservableRedeComplexa;
import Observer.ObserverRedeComplexa;

/**
 * Classe que possui todas as informações da rede complexa, implementada como uma extenção da classe Grafo.
 * A classe estende a classe Grafo e implementa as interfaces Serializabe e ObservableRedeComplexa, 
 * logo é um objeto observável.
 * 
 * Atributos a serem observados:
 *  - Coeficiente de Agregação;
 *  - Densidade;
 *  - Diâmetro;
 *  - Distribuição de Graus: valor da distribuição e lista com a distribuição de cada grau.
 * 
 * @author Lucas Soares, Bruno Menezes, Rennan Rabelo.
 */

public class Rede extends Grafo implements Serializable, ObservableRedeComplexa
{
    private CaminhoMinimo cm;
    private CoefAgregacao calculadorCoefAgregacao;
    private DensidadeGrafo calculadorDensidade;
    private Diametro calculadorDiametro;
    private DistribuicaoGraus calculadorDistribuicaoGraus;
    protected double coefAgregacao;
    protected double densidade;
    protected double diametro;
    protected double distGraus;
    protected ArrayList<Double> listaDistGraus;
    protected HashMap<Integer, Integer> histograma;
    protected ArrayList<ObserverRedeComplexa> observadores;
    
    /**
     * Construtor padrão. 
     * 
     * Instancia todos os atributos usados para calcular as informações da rede.
     */
    public Rede()
    {
        cm = new CaminhoMinimo(this);
        calculadorCoefAgregacao = new CoefAgregacao();
        calculadorDensidade = new DensidadeGrafo();
        calculadorDiametro = new Diametro(this);
        calculadorDistribuicaoGraus = new DistribuicaoGraus(this);
        observadores = new ArrayList<>();        
    }
    
    /**
     * Método que cria uma rede livre de escala.
     * @param tamanhoRede - Indica quantos vértices a deve criada deverá ter.
     * @return Boolean - Retorna o valor true ou falso. 
     */
     public boolean criarRede(int tamanhoRede) {
     
        for(int j = 0; j < tamanhoRede; j++){

             super.adicionarVertice(new Vertice()); // Para não notificar os observadores.
             
             if(j == 1){
                this.getVertice(0).adicionarVizinho(this.getVertice(1));  //quando j=1, sabemos q tem dois vertices, entao eu ja ligo os dois pra comecar a partir dai.
             } else if (j > 1){ // quando j>1, ele calcula a probabilidade de se ligar com cada um dos vertices existentes, se der certo ele se liga.
                 for (int k = 0; k < j; k++){
                     Random gerador = new Random();
                     float numero = gerador.nextFloat();
                     float prob = (float)this.getVertice(k).calcularGrau()/this.grauTotal();
                     
                     if(numero <= prob){
                        this.getVertice(j).adicionarVizinho(this.getVertice(k));                    
                     } else{
                     }
                 }
             }
        }   
        
        calcularAtributos(); // Agora sim calculamos os atributos.
        
        boolean value = false;
        
        return value;
    }
     
     /**
      *  Método que cria uma rede aleatoria
      * @param tamanhoRede - Quantidade de vertices da rede que vai ser gerada.
      * @param prob - probabilidade do nos se conectarem
      * @return 
      */
     public boolean criarRedeAleatoria(int tamanhoRede, float prob) {
                     
        for(int j = 0; j < tamanhoRede; j++){
             this.adicionarVertice(new Vertice());
             
            if (j > 0){ 
                 for (int k = 0; k < j; k++){
                     Random gerador = new Random();
                     float numero = gerador.nextFloat();
                     
                     if(numero <= prob){
                        this.getVertice(j).adicionarVizinho(this.getVertice(k));
                     } else{
                     }
                 }
             
             }
        }   
        
        boolean value = false;
        
        return value;
    }   
     
      /**
     * Método que cria uma rede totalmente aleatória.
     * @param tamanhoRede - Indica quantos vértices a deve criada deverá ter.
     * @return Boolean - Retorna o valor true ou falso. 
     */
     public boolean criarRedeTotalmenteAleatoria(int tamanhoRede) {
                     
        for(int j = 0; j < tamanhoRede; j++){
             this.adicionarVertice(new Vertice());
             
            if (j > 0){ 
                 for (int k = 0; k < j; k++){
                     Random gerador = new Random();
                     float numero = gerador.nextFloat();
                     float prob = gerador.nextFloat();
       
                     
                     if(numero <= prob){
                        this.getVertice(j).adicionarVizinho(this.getVertice(k));
                     } else{
                     }
                 }
             
             }
        }   
        
        boolean value = false;
        
        return value;
    }   
    
     /**
      * Método que calcula o caminho minimo.
      * @param source - Vertice fonte.
      * @return HashTable - Caminho minimo
      */
    public Hashtable<Integer, Integer> calcularCaminhosMinimos(Vertice source)
    {
        try
        {
            cm.setFonte(source);
            cm.setGrafo(this);
            cm.buscaEmLargura();
            
            return cm.distanciaMinima();
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            return null;
       }
    }
    
    /**
     * Método que calcula e retorna todos os predecessores que xistem no caminho minimo.
     * @param source - Vertice fonte.
     * @return ArrayList - Retorna todos os predecessores do caminho minimo
     */
    public ArrayList<Vertice> calcularPredecessoresCaminhoMinimo(Vertice source)
    {
        try
        {
            cm.setFonte(source);
            cm.setGrafo(this);
            cm.buscaEmLargura();
            
            return cm.predecessoresCaminhoMinimo();
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }
    
    /**
     * Atualiza o atribuito referente ao valor do coeficiente de agregação da rede.
     * 
     */
    public void calcularCoefAgregacao()
    {
        coefAgregacao = calculadorCoefAgregacao.coefRede(this);
    }
    
    /**
     * Atualiza o atribuito referente ao valor da densidade da rede.
     * 
     */
    public void calcularDensidade()
    {
        calculadorDensidade.setGrafo(this);
        densidade = calculadorDensidade.calcularDensidade();
    }
    
    /**
     * Atualiza o atribuito referente ao valor do diâmetro da rede.
     * 
     */
    public void calcularDiametro()
    {
        calculadorDiametro.setGrafo(this);
        diametro = calculadorDiametro.calcularDiametro();
    }
    
    /**
     * Atualiza o atribuito referente ao da distribuição de graus da rede.
     * Atualiza o atribuito referente à lista com a distribuição de graus da rede.
     * 
     */
    public void calcularDistribuicaoGraus()
    {
        distGraus = calculadorDistribuicaoGraus.calcularDistribuicao();
        listaDistGraus = calculadorDistribuicaoGraus.calcularDistibuicao();
    }
    
    /**
     * Atualiza o atributo referente ao histograma da rede.
     */
    public void calcularHistograma() {
        histograma = calculadorDistribuicaoGraus.retornarHistograma();
    }
    
    /**
     * Método que atualiza os atributos da rede quando ocorre uma mudança na mesma e notifica seus observadores.
     * São considerados mudanças na rede os seguintes eventos:
     *  - Adicionar ou remover um vértice;
     *  - Adicionar um remover uma aresta.
     * 
     */
    public void calcularAtributos()
    {
        calcularCoefAgregacao();
        calcularDensidade();
        calcularDiametro();
        calcularDistribuicaoGraus();
        calcularHistograma();
        notifyObservers();
    }
    
    /**
     * Método para adicionar um vértice à rede. Sobrescrevemos o método para que os atributos sejam atualizados
     * e os observadores da rede, notificados.
     * 
     * @param v - Vértice a ser adicionado à rede.
     * @return verificar - A variável diz se o vértice foi ou não adicionado à rede.
     * 
     */
    @Override
    public boolean adicionarVertice(Vertice v)
    {
        boolean verificar = this.listaVertices.add(v);
        if(verificar)
        {
            calcularAtributos();
        }
        return verificar;
    }
    
    /**
     * Método para remover um vértice da rede. Sobrescrevemos o método para que os atributos sejam atualizados
     * e os observadores da rede, notificados.
     * 
     * @param v - Vértice a ser removido.
     * @return verificar - Variável que indica se o vértice foi ou não removido.
     * 
     */
    @Override
    public boolean removerVertice(Vertice v)
    {
        boolean verificar = listaVertices.remove(v);
        if(verificar)
        {
            ArrayList<Vertice> vizinhosV = v.getVizinhanca();
            for(Vertice u: vizinhosV)
            {
                u.removerVizinho(v);
            }
            calcularAtributos();
        }
        return verificar;
    }
    
    /**
     * Método que adiciona uma aresta e atualiza os atributos da rede se a remoção ocorre.
     * 
     * @param u - Primeira extremidade da aresta.
     * @param v - Segunda extremidade da aresta.
     * @return verificar - Variável que indica se a adição da aresta ocorreu ou não.
     * 
     */
    @Override
    public boolean adicionarAresta(Vertice u, Vertice v)
    {
        boolean verificar = u.adicionarVizinho(v);
        if(verificar)
        {
            calcularAtributos();
        }
        return verificar;
    }
    
    /**
     * Método que remove uma aresta e atualiza os atributos da rede se a remoção ocorre.
     * 
     * @param u - Primeira extremidade da aresta.
     * @param v - Segunda extremidade da aresta.
     * @return verificar - Variável que indica se a aresta foi ou não removida.
     * 
     */
    @Override
    public boolean removerAresta(Vertice u, Vertice v)
    {
        boolean verificar = v.removerVizinho(u);
        if(verificar)
        {
            calcularAtributos();
        }
        return verificar;
    }
    
    // Métodos que implementam a interface ObservableRedeComplexa.
    
    /**
     * Método para adicionar um novo observador (Observer) à rede.
     * 
     * @param observer - O objeto observer que se deseja adicionar.
     */
    @Override
    public void addObserver(ObserverRedeComplexa observer)
    {
        observadores.add(observer);
    }
    
    /**
     * Método para remover um observador (Observer) da rede.
     * @param observer - O objeto observer que se deseja adicionar.
     */
    @Override
    public void removeObserver(ObserverRedeComplexa observer)
    {
        observadores.remove(observer);
    }
    
    /**
     * Método para notificar os observadores da rede quando ocorre uma mudança de estado.
     * 
     */
    @Override
    public void notifyObservers()
    {
        for(ObserverRedeComplexa ob: observadores)
        {
            ob.update(this);
        }
    }
    
    /**
     * Método para obter o estado do atributo referente ao coeficiente de agreação da rede.
     * @return double - Retorna o valor do coeficiente de agregação da rede.
     */
    @Override
    public double getCoefAgregacao()
    {
        return coefAgregacao;
    }
    
     /**
     * Método para obter o estado do atributo referente à densidade da rede.
     * @return double - Retorna o valor da densidade da rede.
     */
    @Override
    public double getDensidade()
    {
        return densidade;
    }
    
    /**
     * Método para obter o estado do atributo referente ao diâmetro da rede.
     * Diâmetro = maior distância entre os vértices de um grafo.
     * Distância entre vértices de um grafo = menor caminho de um vértice para o outro.
     * @return double - Retorna o valor diâmetro da rede.
     */
    @Override
    public double getDiametro()
    {
        return diametro;
    }
    
    /**
     * Método para obter o estado do atributo referente à distribuição de graus do vértices da rede.
     * @return double - Retorna o valor da distribuição de graus da rede.
     */
    @Override
    public double getDistribuicaoGraus()
    {
        return distGraus;
    }
    
    /**
     * Método para obter o estado do atributo referente à lista de distribuição de graus para cada grau da rede.
     * @return listaDistGraus - Retorna a lista com todas as distribuições de graus.
     */
    @Override
    public ArrayList<Double> getListaDistribuicaoGraus()
    {
        return listaDistGraus;
    }
    
    /**
     * Método que retorna o histograma referente a distribuição dos graus.
     * @return histograma - Histograma, com grau(x) e quant de vertices(y).
     */
    public HashMap<Integer, Integer> getHistograma() {
        return histograma;
    }
     
}
