package Utility;

import javax.swing.JOptionPane;

/**
 * Classe que possui alguns modelos de menssagens utilizadas no sistema.
 * @author Bruno
 */

public class Mensagens
{
    /**
     * Método que retorna uma menssagem com JOption.
     * @param mensagem - Texto com conteúdo da menssagem.
     */
    public static void mensagemAlertaGUI(String mensagem)
    {
        JOptionPane.showMessageDialog(null, mensagem);
    }
    
    /**
     * Método que retorna uma menssagem no Console.
     * @param mensagem - Texto com conteúdo da menssagem.
     */
    public static void mensagemAlertaConsole(String mensagem)
    {
        System.out.println(mensagem);
    }
}
