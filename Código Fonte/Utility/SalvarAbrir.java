package Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import Model.Rede;

/**
 * Classe responsável pelo tratamento de carregar um arquivo .cox, que contém todos os dados de uma rede.
 * @author Felipe Sobreira
 */

public class SalvarAbrir {

    public static void salvarArquivo(Rede rede) {
        try {

            JFileChooser seletor = new JFileChooser();
            seletor.setDialogTitle("Save");
            int userSelection = seletor.showSaveDialog(null);

            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File file = seletor.getSelectedFile();
                FileOutputStream fos;
                ObjectOutputStream oos = null;

                if ((file != null) && file.exists()) {
                    int response = JOptionPane.showConfirmDialog(null,
                            "The file " + file.getName()
                            + " already exists. Do you want to replace the existing file?",
                            "Ovewrite file", JOptionPane.YES_NO_OPTION,
                            JOptionPane.WARNING_MESSAGE);
                    if (response != JOptionPane.YES_OPTION) {
                        fos = new FileOutputStream(file + ".cox");
                        oos = new ObjectOutputStream(fos);
                    }else{
                        oos.close();
                    }
                } else {
                    fos = new FileOutputStream(file + ".cox");
                    oos = new ObjectOutputStream(fos);
                }

                oos.writeObject(rede);
                oos.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /** 
     * Método responsável pelo carregamento de um arquivo que contém a rede.
     * @return Rede - Retorna uma rede para se usar na GUI.
     */
    public static Rede carregarArquivo() {
        Rede rede = null;
        String extensao = null;
        int indicePonto = 0;
        JFileChooser seletor = new JFileChooser();
        seletor.setMultiSelectionEnabled(false); // Assim somente um arquivo pode ser selecionado.  
        seletor.showOpenDialog(null);

        try {
            File file = new File(seletor.getSelectedFile().getAbsolutePath());
            ObjectInputStream input = new ObjectInputStream(new FileInputStream(file));

            extensao = file.getName();
            indicePonto = extensao.lastIndexOf('.');

            if (indicePonto > 0 && indicePonto < extensao.length() - 1) {
                extensao = extensao.substring(indicePonto + 1).toLowerCase();
            }

            if (!extensao.equals("cox")) {        
                return null;
            }

            rede = (Rede) input.readObject();
            input.close();
           
            return rede;

        } catch (IOException ex) {
            //System.out.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            //System.out.println(ex.getMessage());
        }

        return null;

    }
}
