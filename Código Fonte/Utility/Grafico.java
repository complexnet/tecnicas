package Utility;

import java.util.ArrayList;
import java.util.Map;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * Classe responsável por criar o visual do histograma, utiliza a biblioteca do jFreeChart. é necessária utilizar
 * para obter os resultados gráficos.
 * @author Felipe Sobreira
 */
public class Grafico {

    static ChartPanel cp;
    static XYSeries series;
    static XYSeriesCollection dataset;
    static JFreeChart chart;
    static String titulo, metodo;

    /** 
     * Método que seta todos os componentes que serão utilizados na criação do histograma.
     * @param func - Título da janela.
     * @param met - Título do histograma.
     * @param a -  Map contendo o histograma.
     */
    public static void setParametros(String func, String met, Map<Integer,Integer> a) {
        
        ArrayList<Integer> graus = new ArrayList<>();
        for(int i=0 ; i<a.size() ; i++)
            graus.add(i);
        
        titulo = func;
        metodo = met;
        series = new XYSeries(titulo);
        dataset = new XYSeriesCollection();
        dataset.addSeries(series);

        for (int i = 0; i < a.size(); i++) {
         series.add(graus.get(i),a.get(i) );
        }
    }
    /**
     * Método que cria/seta o histograma, com todos os parametros passados.
     */
    public static void setHistogram() {

        chart = ChartFactory.createHistogram(metodo,
                "Grau",
                "Frequência",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                true);
        cp = new ChartPanel(chart);

    }
    
    /**
     * Método que retorna o histograma criado da classe Gráfico
     * @return - Histograma.
     */
    public static ChartPanel getChart() {
        return cp;
    }

}
