/**
 * Interface para implementar um objeto Observer.
 *
 * @author Bruno Menezes.
 */

package Observer;

public interface Observer
{
    public void update(Observable observable);
}