/**
 * Interface para implementar um observador da rede complexa.
 * 
 * @Author Bruno Menezes.
 * 
 */

package Observer;

public interface ObserverRedeComplexa 
{
    public void update(ObservableRedeComplexa observableRede);
}
