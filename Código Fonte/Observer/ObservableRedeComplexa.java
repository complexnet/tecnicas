/**
 * Interface para implementar uma rede complexa observável.
 * 
 * @author Bruno Menezes.
 * 
 */

package Observer;

import java.util.ArrayList;

public interface ObservableRedeComplexa 
{
    public void addObserver(ObserverRedeComplexa observer);
    public void removeObserver(ObserverRedeComplexa observer);
    public void notifyObservers();
    public double getCoefAgregacao();
    public double getDensidade();
    public double getDiametro();
    public double getDistribuicaoGraus();
    public ArrayList<Double> getListaDistribuicaoGraus();
}
