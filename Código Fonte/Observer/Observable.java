/**
 * Interface para implementar um objeto Observable.
 * 
 * @author Bruno Menezes.
 */

package Observer;

public interface Observable
{
    public void addObserver(Observer observer);
    public void removeObserver(Observer observer);
    public void notifyObservers();
}
