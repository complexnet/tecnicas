# Trabalho de Técnicas de Programação #

## Redes Complexas

# PROJETO #

O trabalho visa a criação de um simulador de rede complexa. Inicialmente, o modelo escolhido para apresentar envolve o estudo de redes sociais, mostrando a como se comporta as relações de amizades entre usuários. O modelo também envolve em demonstrar alguns conceitos básicos de redes complexas, a fim de demonstrar seu uso geral.

# ALUNOS ENVOLVIDOS #

Os alunos envolvidos no projeto são:
-Lucas Soares da Rocha
-Bruno Menezes Rocha
-Felipe Sobreira Cassimiro
-Rennan Rabelo da Costa
-Andre Luiz Brizuena