
package Controller;

import Model.Rede;
import Model.Vertice;
import Observer.ObserverRedeComplexa;
import java.io.Serializable;

/**
 * Classe correspondente à implementação de um controlador para a aplicação de Redes Complexas.
 * 
 * @author Bruno Menezes.
 * 
 */
public class ConcreteControllerRedeComplexa implements ControllerRedeComplexa, Serializable
{
    Rede model;
    ControllerViewRedeComplexa view;
    
    /**
     * Construtor vazio do controller da rede complexa.
     */
    public ConcreteControllerRedeComplexa() {}
    
    /**
     * Construtor que inicializa a classe com uma view do controller.
     * @param v - View do controller.
     */
    public ConcreteControllerRedeComplexa(ControllerViewRedeComplexa v)
    {
        view = v;
    }
    
    /**
     * Construtor que seta o modelo do controller com o parametro de uma rede.
     * @param rede - Rede complexa usada no sistema.
     */
    public ConcreteControllerRedeComplexa(Rede rede)
    {
        model = rede;
    }
    
    /**
     * Construtor que seta uma rede e uma view no concreteController.
     * @param rede - Rede complexa usada no sistema.
     * @param v - View do controller
     */
    public ConcreteControllerRedeComplexa(Rede rede, ControllerViewRedeComplexa v)
    {
        model = rede;
        view = v;
        view.setController(this);
        adicionarObservador();
    }
    
    /**
     * Método que adiciona uma rede no controller e adiciona um novo observador na rede.
     * @param model - Rede do utilizada no sistema.
     */
    @Override
    public void setModel(Rede model)
    {
        this.model = model;
        adicionarObservador();
    }
    
    /**
     * Método que retorna a rede contida no controller.
     * @return 
     */
    @Override
    public Rede getModel()
    {
        return this.model;
    }
    
    /**
     * Método que adiciona uma view ao controller.
     * @param view - View do controller.
     */
    public void setView(ControllerViewRedeComplexa view)
    {
        this.view = view;
        adicionarObservador();
    }
    
    /**
     * Método que seta o view como null.
     * @return Boolean - Retornar uma booleano, informando se a operação foi realizada com sucesso ou não. 
     */
    @Override
    public boolean modelNull()
    {
        return model == null;
    }
    
    /**
     * Método que retorna a quantidade de Vértice do modelo.
     * @return Int - quantidade de vértices.
     */
    @Override
    public int quantidadeVertices()
    {
        return model.quantVertices();
    }
    
    /**
     * Método que adiciona um vértice ao modelo.
     * @param v -  Vértice qualquer.
     * @return Boolean 
     */
    @Override
    public boolean adicionarVertice(Vertice v)
    {
        return model.adicionarVertice(v);
    }
    
    /**
     * Método que remove um vértice do modelo.
     * @param v - Vértice qualquer.
     * @return Boolean
     */
    @Override
    public boolean removerVertice(Vertice v)
    {
        return model.removerVertice(v);
    }
    
    /**
     * Método que remove um vértice do modelo.
     * @param index - Índice do vértice.
     * @return Boolean
     */
    @Override
    public boolean removerVertice(int index)
    {
        return model.removerVertice(index);
    }
    
    /**
     * Método que adicionar uma aresta ao modelo.
     * @param v - Vértice qualquer.
     * @param u - Vértice qualquer.
     * @return Boolean
     */
    @Override
    public boolean adicionarAresta(Vertice v, Vertice u)
    {
        return model.adicionarAresta(v, u);
    }
    
    /**
     * Método que remove uma aresta do modelo.
     * @param v - Vértice qualquer.
     * @param u - Vértice qualquer.
     * @return Boolean
     */
    @Override
    public boolean removerAresta(Vertice v, Vertice u)
    {
        return model.removerAresta(v, u);
    }
    
    /**
     * Método que recupera um vértice do modelo.
     * @param index - Índice do vértice desejado.
     * @return Vertice
     */
    @Override
    public Vertice getVertice(int index)
    {
        return model.getVertice(index);
    }
    
    /**
     * Método que chama o método main da view(rede). E atualiza o módelo.
     */
    @Override
    public void processInput()
    {
        model.calcularAtributos();
        view.main();
    }
    
    /**
     * Método responsável por adicionar os observadores. 
     */
    private void adicionarObservador()
    {
        if(model != null && view != null)
        {
            if(view instanceof ObserverRedeComplexa)
            {
                model.addObserver((ObserverRedeComplexa) view);
            }
        }
    }
}
