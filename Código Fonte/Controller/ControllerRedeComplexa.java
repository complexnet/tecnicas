/**
 * Interface para implementar um controle para uma rede complexa.
 * 
 * @author Bruno Menezes.
 */

package Controller;

import Model.Rede;
import Model.Vertice;

public interface ControllerRedeComplexa
{
    public void setModel(Rede model);
    public Rede getModel();
    public void setView(ControllerViewRedeComplexa view);
    public boolean modelNull();
    public int quantidadeVertices();
    public boolean adicionarVertice(Vertice v);
    public boolean removerVertice(Vertice v);
    public boolean removerVertice(int indexVertice);
    public boolean adicionarAresta(Vertice v, Vertice u);
    public boolean removerAresta(Vertice v, Vertice u);
    public Vertice getVertice(int indexVertice);
    public void processInput();
}
