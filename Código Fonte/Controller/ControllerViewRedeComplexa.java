/**
 * Interface para uma View que deve ser usada por um controlador da aplicação.
 * 
 * @author Bruno Menezes.
 */

package Controller;

public interface ControllerViewRedeComplexa
{
    public void main();
    public void setController(ControllerRedeComplexa controller);
}
