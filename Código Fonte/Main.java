/**
 * Corresponde à classe principal do programa.
 * 
 * @author Bruno Menezes
 */

import Controller.ConcreteControllerRedeComplexa;
import Controller.ControllerRedeComplexa;
import Controller.ControllerViewRedeComplexa;
import GUI.TelaInicial;
import Model.Rede;


public class Main
{
    public static void main(String args[])
    {
        ControllerViewRedeComplexa telaInicial = new TelaInicial();
        Rede rede = new Rede();
        ControllerRedeComplexa controller = new ConcreteControllerRedeComplexa(rede, telaInicial);
        telaInicial.setController(controller);
        controller.processInput();
          
    }
}
